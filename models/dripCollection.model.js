
const mongoose = require('mongoose');
const Schema = mongoose.Schema
var DripCollectionSchema = new mongoose.Schema({
dripId:{
  type: Schema.ObjectId,
  ref: 'DripCampaign'
},
agent:{
  type: Schema.ObjectId,
  ref: 'Agents'
},
title: String,
mailCampaign:{
  type: Schema.ObjectId,
  ref: 'MailCampaign'
},
sequence:{
  type: Schema.ObjectId,
},
user:{
  type: Schema.ObjectId,
  ref: 'User'
},
datetime:String,
sequenceType:String,
sourceCampaign:{
  type:Schema.ObjectId,
  ref: 'MailCampaign'
},
campType : String,
stage: String,
filter: String,
date: String,
attachments :[],
time: String,
body: String,
drip_camp_mode:String,
signature: String,
template: String,
unsubscribe :{
  type:Boolean,
  default : false
},
scheduleType:String,
after : String,
afterCount : String,
alreadySentWithSeq:{
  type:Boolean,
  default: false
},
mode : String,
senderEmail:String,
jobName:String,
subject:String,
successStatus :{
  type: String,
  default: 'Not Started'
},
agendaId:{
  type: Schema.ObjectId,
  ref: 'agendaJobs'
},
archived:{
   type:Boolean,
   default:false
},

createdAt:{
  type:Date,
  default:Date.now
},
tags : [
    {
       realKey : String,
       dbKey : String
    }
],
selectedTemplate : String,
selectedSignature : String
});
// require('./dripCollection.events').applyHooks(DripCollectionSchema);
module.exports = mongoose.model('DripCollection', DripCollectionSchema);
