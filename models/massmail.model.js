'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema

var MassmailSchema = new mongoose.Schema({
  send: Object,  // email send status, it used in to provide total count of send emails to agent
  open : [],    // email open status, it used in to provide total count of open emails to agent
  complaint:[], // email spam status, it used in to provide total count of spam emails to agent
  delivery:Object,   // email delivery status, it used in to provide total count of delivered emails to agent
  click:[],        // email click status, it used in to provide total count of clicked emails to agent
  bounce:Object,   // email bounce status, it used in to provide total count of bounced emails to agent
  messageId : String,  //provided by AWS
  mailbody : String,  //html template
  mailCampaignId :  {
    type: Schema.ObjectId,
    ref: 'MailCampaign'
  },  // campaign id of the dripCampaign 
  dripCollectionId :  {
    type: Schema.ObjectId,
    ref: 'DripCollection'
  },  //stage id of the dripCampaign
  agent: {
    type: Schema.ObjectId,
    ref: 'Agents'
  },
  sequenceUserId : {
    type : Schema.ObjectId,
    ref : 'MassMailList'
  },  //id of list , which is used to target the users for a campaign
  uniqueKey:String,
  contact : {
    type  : Schema.ObjectId ,
    ref : 'ContactList'
  },
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },  // id of the targeted user
  external_source_info : {
    type: Schema.ObjectId,
    ref: 'Externalcontactinfo'
  },
  dealboardcard_id : {
    type: Schema.ObjectId,
    ref: 'Dealboardcard'
  },
  recentEvent:String,
  reject : Boolean,// email spam status, it used in to provide total count of spam emails to agent
  info: String,
  source  : String, //email id of sender
  mailType:String, //it is used to differentiate between campaigns
  workflow_id:{
    type: Schema.ObjectId,
    ref: 'Workflow'
  }, //id of the workflow automation
  workflow_date:String,  
  destination : String, // user's email address
  active: Boolean, 
  campmessage_state: String,  
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  },
  archived : {
    type: Boolean,
    default: false
    
  },
  archivedAt: {
    type: Date
  },
  trackingType:String,
  tracking_date:Date
});

module.exports= mongoose.model('Massmail', MassmailSchema);
