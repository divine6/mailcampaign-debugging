'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema

var MassMailListSchema = new mongoose.Schema({
  email: String,
  name : String,
  phone:String,
  expiration_date : String,
  carrier_name : String,
  sequence : [{
    type: Schema.ObjectId,
    ref: 'Sequence'
    }
  ],
  contact : {
    type: Schema.ObjectId,
    ref: 'ContactList'
  },
  user : {
    type: Schema.ObjectId ,
    ref : 'User'
  },
  agent: {
    type: Schema.ObjectId,
    ref: 'Agents'
  },
  unsubscribe : {
    type : Boolean,
    default : false
  },
  smsunsubscribe: {
    type : Boolean,
    default : false
  },
  active: Boolean,  
  createdAt: {
    type: Date,
    default: Date.now
  },
  updatedAt: {
    type: Date
  }

});

module.exports= mongoose.model('MassMailList', MassMailListSchema);
