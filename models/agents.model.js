

const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema


const authTypes = ['github', 'twitter', 'facebook', 'google'];

var AgentsSchema = new Schema({
    agency_id: {
        type: Schema.ObjectId,
        ref: 'Agency'
    },
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    user_status: {
        type: String,
        default: 0,
    },
    
    user: [{
        type: Schema.ObjectId,
        ref: 'User',

    }],
    access_agents:[{
        type: Schema.ObjectId,
        ref: 'Agents',
    }],
    access_csr:[{
        type: Schema.ObjectId,
        ref: 'Agents',
    }],
    amsInfo : {
        type: Schema.ObjectId,
        ref: 'Amsinfo',
    },
    amsPass : String,
    amsUserName : String,
    phone: {
        type: String,
    },
    phone_cell: {
        type: String,
    },
    dob: {
        type: String,
    },
    address: {
        type: String,
    },
    coverage_provide: [
        {
            type: String,
        }
    ],
    companies_represent: [
        {
            type: String,
        }
    ],
    states_served: [
        {
            type: String,
        }
    ],
    city: {
        type: String,
    },
    mailClient:String,
    agent_image:{
        type : String
    },
    state: {
        type: String,
    },
    zip: {
        type: String,
    },
    about_me: {
       type : String
    },
    outlook_access_token : {
        type : String
    },
    outlook_user_name : {
        type : String
    },
    twilioNumber:{
        type: String
    },
    allowReferProgram : {
        type : Boolean,
        default : true

    },
    outlook_refresh_token : {
        type : String
    },
    outlook_token_created : {
        type : Date
    },
    web_token: {
        type : Object
    },
    email: {
        type: String,
        lowercase: true,
        sparse : true,
        index:true,
        unique:true,
        required() {
            if (authTypes.indexOf(this.provider) === -1) {
                return true;
            } else {
                return false;
            }
        }
    },
    refreshdate : {
        type : Date
    },
    twilio: {
        number: String,
        authToken:String,
        accountId:String
    },
    mailInfo:{
      email:String,
      password:String,
      hostname:String
    },
    agent_role: {
        type: String,
        default: 'agent'
    },
    subRoles : [{
        type : String,
        default : 'agent'
    }],
    role: {
        type: String,
        default: 'agent'
    },
    dealboardView: {
        type: String,
        default: 'grid'
    },
    autotaskView: {
        type: String,
        default: 'grid'
    },
    nylas_token : {
        type : String
    },
    password: {
        type: String,
        required() {
            if (authTypes.indexOf(this.provider) === -1) {
                return true;
            } else {
                return false;
            }
        }
    },
    linkedinId: {
        type: String
    },
    googleId: {
        type: String
    },
    facebookId: {
        type: String
    },
    reports : [],
    requiredModule : [],
    modulesNotRequired:[],
    provider: {
        type : String,
        default : 'local'
    },
    nylas_account_id:{
        type : String,
    },
    mailServerType:{
        type: String,
        default : "aws"
    },
    mailgunDomain:String,
    register_date: {
        type: Date,
        default: Date.now
    },
    mailCampaignActiveEmailList : [],
    appdrip_trigger:{
        type : Boolean,
        default:false
    },
    trigger_date:{
        type: Date
    },
    archived: {
       type: Boolean,
       default: false
    },
    archivedAt: {
      type : Date
    },
    last_login_date: {
        type: Date,
        default: Date.now
    },
    salt: String,
    mailClient: String,
    agentUrl : String, 
    facebook: {},
    google: {},
    github: {}
},{ usePushEach: true });

/**
 * Virtuals
 * 
 */

// Public profile information
AgentsSchema
    .virtual('profile')
    .get(function () {
        return {
            name: this.name,
            role: this.role
        };
    });

// Non-sensitive info we'll be putting in the token
AgentsSchema
    .virtual('token')
    .get(function () {
        return {
            _id: this._id,
            role: this.role
        };
    });

/**
 * Validations
 */

// Validate empty email
AgentsSchema
    .path('email')
    .validate(function (email) {
        if (authTypes.indexOf(this.provider) !== -1) {
            return true;
        }
        return email.length;
    }, 'Email cannot be blank');

// Validate empty password
AgentsSchema
    .path('password')
    .validate(function (password) {
        if (authTypes.indexOf(this.provider) !== -1) {
            return true;
        }
        return password.length;
    }, 'Password cannot be blank');

// Validate email is not taken
// AgentsSchema
//     .path('email')
//     .validate(function (value) {
//         if (authTypes.indexOf(this.provider) !== -1) {
//             return true;
//         }

//         return this.constructor.findOne({ email: value }).exec()
//             .then(user => {
//                 if (user) {
//                     if (this.id === user.id) {
//                         return true;
//                     }
//                     return false;
//                 }
//                 return true;
//             })
//             .catch(function (err) {
//                 throw err;
//             });
//     }, 'The specified email address is already in use.');

var validatePresenceOf = function (value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
AgentsSchema
    .pre('save', function (next) {
        // Handle new/update passwords
        if (!this.isModified('password')) {
            return next();
        }

        if (!validatePresenceOf(this.password)) {
            if (authTypes.indexOf(this.provider) === -1) {
                return next(new Error('Invalid password'));
            } else {
                return next();
            }
        }

        // Make salt with a callback
        this.makeSalt((saltErr, salt) => {
            if (saltErr) {
                return next(saltErr);
            }
            this.salt = salt;
            this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
                if (encryptErr) {
                    return next(encryptErr);
                }
                this.password = hashedPassword;
                return next();
            });
        });
    });

/**
 * Methods
 */
AgentsSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate(password, callback) {
        if (!callback) {
            return this.password === this.encryptPassword(password);
        }

        this.encryptPassword(password, (err, pwdGen) => {
            if (err) {
                return callback(err);
            }

            if (this.password === pwdGen) {
                return callback(null, true);
            } else {
                return callback(null, false);
            }
        });
    },

    /**
     * Make salt
     *
     * @param {Number} [byteSize] - Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt(...args) {
        let byteSize;
        let callback;
        let defaultByteSize = 16;

        if (typeof args[0] === 'function') {
            callback = args[0];
            byteSize = defaultByteSize;
        } else if (typeof args[1] === 'function') {
            callback = args[1];
        } else {
            throw new Error('Missing Callback');
        }

        if (!byteSize) {
            byteSize = defaultByteSize;
        }

        return crypto.randomBytes(byteSize, (err, salt) => {
            if (err) {
                return callback(err);
            } else {
                return callback(null, salt.toString('base64'));
            }
        });
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword(password, callback) {
        if (!password || !this.salt) {
            if (!callback) {
                return null;
            } else {
                return callback('Missing password or salt');
            }
        }

        var defaultIterations = 10000;
        var defaultKeyLength = 64;
        var salt = new Buffer(this.salt, 'base64');

        if (!callback) {
            // eslint-disable-next-line no-sync
            return crypto.pbkdf2Sync(password, salt, defaultIterations,
                defaultKeyLength, 'sha1')
                .toString('base64');
        }

        return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength,
            'sha1', (err, key) => {
                if (err) {
                    return callback(err);
                } else {
                    return callback(null, key.toString('base64'));
                }
            });
    }
};
    
module.exports = mongoose.model('Agents', AgentsSchema);