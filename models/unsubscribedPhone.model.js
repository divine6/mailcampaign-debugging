
const mongoose = require("mongoose")
const Schema = mongoose.Schema;

var UnsubscribedPhoneSchema = new mongoose.Schema({
  name: String,
  phone: String,
  active: Boolean,
  agent: {
    type: Schema.ObjectId,
    ref: 'Agents',
  },
  createdAt :{
    type: Date,
    default: Date.now
  }
});

module.exports=mongoose.model('UnsubscribedPhone',UnsubscribedPhoneSchema)
