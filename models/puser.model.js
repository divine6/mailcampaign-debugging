const mongoose = require("mongoose")
const updateVersioningPlugin = require('mongoose-update-versioning');

const Schema = mongoose.Schema


const UserSchema = new Schema({
    emailid : String,
    username: String,
    friends : [{
        name : String
    }]
})
UserSchema.plugin(updateVersioningPlugin);

module.exports=mongoose.model('puser',UserSchema)