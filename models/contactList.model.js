'use strict';
const mongoose =require('mongoose');
const Schema =mongoose.Schema
var ContactListSchema = new mongoose.Schema({
  firstname: String,
  name:String,
  email: String,
  mobile: String,
  phone : String, 
  address :String,
  city : String,
  state : String,
  zip : String,
  age : String,
  gender : String,
  designation: String,
  type:String,
  company_id: {    
    type: Schema.ObjectId,    
    ref: 'Companie'  
  },
  // user: {
  //   type: Schema.ObjectId,
  //   ref: 'User',

  // },
  agent:{
    type: Schema.ObjectId,
    ref: 'Agents',
} ,
createdAt : {
  type : Date,
  default : Date.now
},
modifiedAt : {
  type : Date
}
});

module.exports= mongoose.model('ContactList', ContactListSchema);
