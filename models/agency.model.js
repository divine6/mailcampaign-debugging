/* eslint-disable camelcase */
    'use strict';

    const mongoose =require('mongoose');
    const Schema =mongoose.Schema
    
    var AgencySchema = new mongoose.Schema({
      insurence: {
        type: Schema.ObjectId,
        ref: 'Insurance'
      },
      redirectSMS :{
        type:Boolean,
        default:false
      },
      agency_support_phone :String,
      agency_icon: String, /**Agency logo. It is used to whitelisting their communication like emails and also visible to their public web page provided by insuredmine */
      agency_name: {
        type: String, /**Name of the agency */
      },
      agent_phoneNo: {
        type: String, /** Not in use. */
      },
      agency_phoneNo: {
        type: String, /**Phone number to contact agency support */
      },
      salesforceTokenGetApi: {
        type: String     /**There are some salesforce based  CRM used  by agency to manage their Insurance data. It store the token for their system integration */
      },
      salsforceActivePolicyApi: {
        type: String
      },
      salesforceActiveClientsApi: {
        type: String
      },
      agency_address: {
        type: String /**Address of the agency headquarter */
      },
      agency_website: {
        type: String /**Current running website */
      },
      facebookId: {
        type: String /**Facebook page link */
      },
      agencytoken: {
        type: String  
      },
      twitterId: {
        type: String
      },
      linkedinId: {
        type: String
      },
      agency_email: {
        type: String
      },
      agency_support_email: {
        type: String   /**Support email */
      },
      about_us: String,
      coverage_provide: [
        {
          type: String,  /**Covergae provided by the company. Like Auto/Home/Business etc*/
        }
      ],
      states_served: [
        {
          type: String /**States in which agency provides it's services */
        }
      ],
      companie_represent: [
        {
          type: String /**Carrier for which they sold policies */
        }
      ],
      business_hours: [
        {
          type: String
        }
      ],
      subDomain: {
        type: String    /**Subdomai like abc.insuredmine.com to access their agent/customer portal */
      },
      verificationHash: {
        type: String
      },
      domainVerified: {
        type: Boolean,
        default: false
      },
      createdAt: {
        type: Date,
        default: Date.now
      },
      updatedAt: {
        type: Date
      },
      onboarding_date: {
        type: Date
      },
      appdrip_trigger: {
        type: Boolean,
        default: false   /**Parameter to handle drip email for those who downloads looged in into their app */
      },
      default_stage: {
        type: Schema.ObjectId,
        ref: 'Dealboard'  /**We get propect data from extrnal link resource. Those data will feed into dealboard. This param denotes the deafult deal stage for the agency to push the deal info which comes from external resources */
      },
      default_Renewal_board: {
        type: Schema.ObjectId,
        ref: 'Dealboardinfo'  /**We get propect data from extrnal link resource. Those data will feed into dealboard. This param denotes the deafult deal stage for the agency to push the deal info which comes from external resources */
      },
      timezone: {
        date_format: {
          type: String,
          default: "MM-dd-yyyy" 
        }
      },
      dafault_board: {
        type: Schema.ObjectId,
        ref: 'Dealboardinfo'  /**Default baord id in which external info pushed */
      },
      get_quotes: {
        type: Boolean,
        default: false
      },
      country_code : String,
      premium_feature: {
        type: Object    /**Not in use as of now */
      },
      amsLink: String, /**From whihc AMS agency currently linked */
      ams360APICreds : {  /**AMS360 is an ams. To access data through API we use these creds */
        type  :Object
      },
      amsInfo : {
        type: Schema.ObjectId,
        ref: 'Amsinfo',
    },
      ams360NotificationKey : String,
      quotesheetfields: String,
      quoteLang : String,
      qqcatalystCreds : {
        type :Object
      },
      appDrip:{
        playstoreLink:String,
        iosLink:String
      },
      quotesheetfields: String,
      archived : Boolean ,
      dailySync : {type : Boolean, default : true}, //Parameters to stop dailysync for existing QQ customer on whom QQ  ID's not mapped
      archivedAt : {
        type : Date
      }
    });

    module.exports=mongoose.model('Agency', AgencySchema);