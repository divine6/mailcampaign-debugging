'use strict';
const mongoose =require('mongoose');
const Schema =mongoose.Schema
var CategoriesSchema = new mongoose.Schema({
    category_name: {
        type: String,
        required:true
    },
    category_details: {
        type: String,
    },
    iconUrl: {
        type: String,
    },
    bannerUrl: {
        type: String,
    },
    insurance_type : [
        {type : String}
    ],
    category_status: {
        type: String,
    },
    category_array: [{
        type:String
    }],
    category_add_date: {
        type: Date,
        default: Date.now
    },
    refreshdate: {
        type: Date
    }
});
module.exports=mongoose.model('Categories', CategoriesSchema);