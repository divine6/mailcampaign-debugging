'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema

var SequenceSchema = new mongoose.Schema({
  name: String,
  info: String,
  type: String,
  mailCampaign : [{
    type: Schema.ObjectId,
    ref: 'MailCampaign'
    }
  ],
  agent: {
    type: Schema.ObjectId,
    ref: 'Agents'
  },
  archived : {
    type : Boolean ,
    default : false
  },
  visibility_role:{
    type : String,
    default : 'agent'
  },
  sequenceFilter:{},
  listType : String,
  archivedAt : {
    type : Date
  },
  createdAt : {
    type : Date ,
    default : Date.now
  },
  totalCount : Number ,
  totalPhoneCount : Number, 
  active: Boolean
});

module.exports= mongoose.model('Sequence', SequenceSchema);
