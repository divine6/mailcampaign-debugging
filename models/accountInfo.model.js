'use strict';



const mongoose = require('mongoose');
const Schema = mongoose.Schema

var AccountInfoSchema = new mongoose.Schema({
  account_name: String,
  account_info: String,
  account_type : String,
  emergency_name : String,
  emergency_number : String,
  last_updated_field : String,
  account_status : String,
  refreshsession : String,
  agent : {
    type: Schema.ObjectId,
    ref: 'Agent'
  },
  agency_id : {
   type: Schema.ObjectId,
   ref:'Agency'
  }
});

module.exports = mongoose.model('AccountInfo', AccountInfoSchema);
