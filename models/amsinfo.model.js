'use strict';
const mongoose = require('mongoose');
const Schema = mongoose.Schema
var AmsinfoSchema = new mongoose.Schema({
  name: String,
  info: String,
  image : String,
  active: Boolean
});

module.exports = mongoose.model('Amsinfo', AmsinfoSchema);
