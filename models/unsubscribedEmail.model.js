
const mongoose = require("mongoose")
const Schema = mongoose.Schema;
var UnsubscribedEmailSchema = new mongoose.Schema({
  name: String,
  email: String,
  active: Boolean,
  agent: {
    type: Schema.ObjectId,
    ref: 'Agents',
  },
  createdAt :{
    type: Date,
    default: Date.now
  }
});

module.exports=mongoose.model('UnsubscribedEmail',UnsubscribedEmailSchema)
