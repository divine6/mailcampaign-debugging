'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema
var CompanieSchema = new mongoose.Schema({
    company_name: {
        type: String
    },
    company_code: {
        type: String,
    },
    company_img: {
        type: String,
    },
    company_website: {
        type: String,
    },
    company_status: {
        type: String,
    },
    customer_service: {
        type: String,
    },
    company_email : {
        type: String,
    },
    claim_website : {
        type : String
    },
    claim_email : {
        type : String
    },
    claim_service : {
        type : String
    },
    premium_website : {
        type : String
    },
    ams360CompanyCode : String,
    premium_email : {
        type : String
    },
    premium_service : {
        type : String
    },
    coverages : [],
    categories: {
        type: Schema.ObjectId,
        ref: 'Categories'
    },
    allcategories: [{
        type: Schema.ObjectId,
        ref: 'Categories'
    }],
    company_about : {
        type : String
    },
    company_add_date: {
        type: Date,
        default: Date.now
    },
    company_array: [],
    updatedAt: {
        type: Date
    },
    createdAt : { 
        type : Date ,
        default : Date.now
    }   
});

module.exports= mongoose.model('Companie', CompanieSchema);