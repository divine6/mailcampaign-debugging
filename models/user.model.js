'use strict';
/*eslint no-invalid-this:0*/
const crypto =require('crypto');
const mongoose =require('mongoose');
const Schema =mongoose.Schema

const authTypes = ['github', 'twitter', 'facebook', 'google'];

var UserSchema = new Schema({
    firstname: {
        type: String,
    },
    lastname: {
        type: String,
    },
    name: {
        type: String,
    },
    user_status: {
        type: String,
        default: 0,
    },
    about_us : {
        type : String
    },
    status: {
        type: String,
        default: 0,
    },
    user_type :{
        type : String,
        default : '0'
    },
    archived:{
        type:Boolean,
        default:false
    },
    agent: [{
        type: Schema.ObjectId,
        ref: 'Agents',
       
      }],
      agency_id: {
        type: Schema.ObjectId,
        ref: 'Agency',
       
      },
    signalId: {
        type: String
    },
    account_id : [{
        type : Schema.ObjectId,
        ref : 'AccountInfo'
    }],
    otp: {
        type: String,
    },
    phone: {
        type: String,
    },
    phone_cell: {
        type: String,
    },
    emergencyNumber : {
        type : String
    },
    emergencyContactName : {
        type : String
    },
    bloodGroup : {
        type : String
    },
    dob: {
        type: String,
    },
    city: {
        type: String,
    },
    state: {
        type: String,
    },
    userType:String,
    stateId: {
        type: Schema.ObjectId,
        ref: 'State',

    },
    device_token : {
        type : String
    },
    referralCode : {
        type : String
    },
    referredByCode : {
        type :String
    },
    zip: {
        type: String,
    },
    user_image: {
        type: String,
    },
    publicId: {
        type: String,
    },
    address : {
        type : String
    },
    company_id: {
        type: Schema.ObjectId,
        ref: 'Companie'
    },
    address2 : {
        type : String
    },
    appVersion : String,
    email: {
        type: String,
        lowercase: true,
        // required() {
        //     if (authTypes.indexOf(this.provider) === -1) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }
    },
    role: {
        type: String,
        default: 'user'
    },
    password: {
        type: String,
        // required() {
        //     if (authTypes.indexOf(this.provider) === -1) {
        //         return true;
        //     } else {
        //         return false;
        //     }
        // }
    },
    linkedinId: {
        type: String
    },
    googleId: {
        type: String
    },
    facebookId: {
        type: String
    },
    provider: String,
    register_date: {
        type: Date,
    },
    last_login_date: {
        type: Date,
    },
    agent_onboarding_date:{
        type:Date
    },
    createdAt :{
        type: Date,
        default: Date.now
    },
    active : {
        type : Boolean,
        default :false
    },
    hasActivePolicy : {
        type : Boolean,
        default : false
    },
    userType : {
        type:String,
        default: "Other"
    } ,
    refreshsession : String,
    userSubType : [],
    designation : String ,
    refreshdate : String,
    unsubscribe : {
        type : Boolean,
        default : false
    },
    smsunsubscribe : {
        type : Boolean,
        default : false
    },
    verificationHash :{
        type : String
    },
    forgotPasswordHash :{
        type : String
    },
    org : String, 
    userAmountForRefer : {
        type : Number,
        default :10
    },
    organisation_name : {
        type : String
    },
    primary : {
        type : Boolean,
        default : false
    },
    web_subscription_id:{
        type: String
    },
    salt: String,
    about_us : String, 
    quoteGenerated : {type : Boolean, default : false}, 
    facebook: {},
    picture:{},
    events:{},
     tagged_places:{},
     birthday:{
         type : Date
     },
     gender : String,
     hometown:{},
     feed:{},
     life_event:[],
     location:{},
     realationship_status : String,
     signupPlatform : String, //To detect on which platform client uses our services ios/android/web  
     policy_coverage_commercial :[{
         category : {
            type: Schema.ObjectId,
            ref: 'Categories',
        },
        status: {
            type: Boolean,
            default: false
        }
    }],
    policy_coverage_personal :[{
        category : {
           type: Schema.ObjectId,
           ref: 'Categories',
       },
       status: {
           type: Boolean,
           default: false
       }
   }],
   show_add_policy_coverage_popup:{
       type :Boolean , 
       default : true
   },
     chatbotFeedback : Boolean,

    google: {},
    github: {}
});

/**
 * Virtuals
 */

// Public profile information
UserSchema
    .virtual('profile')
    .get(function() {
        return {
            name: this.name,
            role: this.role
        };
    });

// Non-sensitive info we'll be putting in the token
UserSchema
    .virtual('token')
    .get(function() {
        return {
            _id: this._id,
            role: this.role
        };
    });

/**
 * Validations
 */

// Validate empty email
UserSchema
    .path('email')
    .validate(function(email) {
        if (authTypes.indexOf(this.provider) !== -1) {
            return true;
        }
        return email.length;
    }, 'Email cannot be blank');

// Validate empty password
UserSchema
    .path('password')
    .validate(function(password) {
        if (authTypes.indexOf(this.provider) !== -1) {
            return true;
        }
        return password.length;
    }, 'Password cannot be blank');

// Validate email is not taken
// UserSchema
//     .path('email')
//     .validate(function(value) {
//         if (authTypes.indexOf(this.provider) !== -1) {
//             return true;
//         }

//         return this.constructor.findOne({ email: value }).exec()
//             .then(user => {
//                 if (user) {
//                     if (this.id === user.id) {
//                         return true;
//                     }
//                     return false;
//                 }
//                 return true;
//             })
//             .catch(function(err) {
//                 throw err;
//             });
//     }, 'The specified email address is already in use.');

var validatePresenceOf = function(value) {
    return value && value.length;
};

/**
 * Pre-save hook
 */
UserSchema
    .pre('save', function(next) {
        // Handle new/update passwords
        if (!this.isModified('password')) {
            return next();
        }

        if (!validatePresenceOf(this.password)) {
            if (authTypes.indexOf(this.provider) === -1) {
                return next(new Error('Invalid password'));
            } else {
                return next();
            }
        }

        // Make salt with a callback
        this.makeSalt((saltErr, salt) => {
            if (saltErr) {
                return next(saltErr);
            }
            this.salt = salt;
            this.encryptPassword(this.password, (encryptErr, hashedPassword) => {
                if (encryptErr) {
                    return next(encryptErr);
                }
                this.password = hashedPassword;
                return next();
            });
        });
    });

/**
 * Methods
 */
UserSchema.methods = {
    /**
     * Authenticate - check if the passwords are the same
     *
     * @param {String} password
     * @param {Function} callback
     * @return {Boolean}
     * @api public
     */
    authenticate(password, callback) {
        if (!callback) {
            return this.password === this.encryptPassword(password);
        }

        this.encryptPassword(password, (err, pwdGen) => {
            if (err) {
                return callback(err);
            }

            if (this.password === pwdGen) {
                return callback(null, true);
            } else {
                return callback(null, false);
            }
        });
    },

    /**
     * Make salt
     *
     * @param {Number} [byteSize] - Optional salt byte size, default to 16
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    makeSalt(...args) {
        let byteSize;
        let callback;
        let defaultByteSize = 16;

        if (typeof args[0] === 'function') {
            callback = args[0];
            byteSize = defaultByteSize;
        } else if (typeof args[1] === 'function') {
            callback = args[1];
        } else {
            throw new Error('Missing Callback');
        }

        if (!byteSize) {
            byteSize = defaultByteSize;
        }

        return crypto.randomBytes(byteSize, (err, salt) => {
            if (err) {
                return callback(err);
            } else {
                return callback(null, salt.toString('base64'));
            }
        });
    },

    /**
     * Encrypt password
     *
     * @param {String} password
     * @param {Function} callback
     * @return {String}
     * @api public
     */
    encryptPassword(password, callback) {
        if (!password || !this.salt) {
            if (!callback) {
                return null;
            } else {
                return callback('Missing password or salt');
            }
        }

        var defaultIterations = 10000;
        var defaultKeyLength = 64;
        var salt = new Buffer(this.salt, 'base64');

        if (!callback) {
            // eslint-disable-next-line no-sync
            return crypto.pbkdf2Sync(password, salt, defaultIterations,
                    defaultKeyLength, 'sha1')
                .toString('base64');
        }

        return crypto.pbkdf2(password, salt, defaultIterations, defaultKeyLength,
            'sha1', (err, key) => {
                if (err) {
                    return callback(err);
                } else {
                    return callback(null, key.toString('base64'));
                }
            });
    }
};

module.exports = mongoose.model('User', UserSchema);