'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema
var MailCampaignSchema = new mongoose.Schema({
  title: String,
  recipients: Number,
    agent: {
      type: Schema.ObjectId,
      ref: 'Agents'
    },
  mailbody : {
    type : String
  },
  sequence : {
    type : Schema.ObjectId,
    ref : 'Sequences'
  },
  user : {
    type : Schema.ObjectId,
    ref : 'User'
  },
  dripId:{
    type : Schema.ObjectId,
    ref : 'DripCampaign'
  },
  workflow_id:{
    type : Schema.ObjectId,
    ref : 'workflow'
  },
  campType: String,
  datetime:String,
  attachments :[],
  errorStatus : String,
  sequenceType:String,
  scheduleType:String,
  sms_template: String,
  after : String,
  afterCount : String,
  sourceCampaign:{
    type:Schema.ObjectId,
    ref: 'MailCampaign'
  },
  stop:{
    type:Boolean,
    default : false
  },
  stage: String,
  filter: String,
  date: String,
  time: String,
  body: String,
  signature: String,
  template: String,
  unsubscribe :{
    type:Boolean,
    default : false
  },
  mode : String,
  senderEmail:String,
  jobName:String,
  subject:String,
  drip_camp_mode:String,
  successStatus :{
    type: String,
    default: 'Not Started'
  },
  external_source_info : {
    type: Schema.ObjectId,
    ref: 'Externalcontactinfo'
  },
  dealboardcard_id : {
    type: Schema.ObjectId,
    ref: 'Dealboardcard'
  },
  sequenceType : String,
  createdAt: {
    type : Date,
    default: Date.now
  },
  scheduleDate :{
    type : Date
  },
  status : {
    type : Boolean, 
    default : true
  },
  archivedAt :{
    type : Date
  },
  selectedTemplate : String,
  selectedSignature : String
});

module.exports= mongoose.model('MailCampaign', MailCampaignSchema);
