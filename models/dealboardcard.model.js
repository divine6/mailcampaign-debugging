'use strict';

const mongoose =require('mongoose');
const Schema =mongoose.Schema
var DealboardcardSchema = new mongoose.Schema({

  active: Boolean, 
  organisationName : String ,
  dealTitle : String ,//what is the title of the card
  value : String ,//what is the value of the deal
  dealInfo : String,
  dealboard : {
    type : Schema.ObjectId ,
    ref : 'Dealboard' 

  },//stage id to differntiate which card is related to which stage
  source: {
    type: String
  },//what is the source of this card (from website or from portal)
  user: {
    type: Schema.ObjectId,
    ref: 'User'
  },//user id (this card is related to which user)
  label_id: [{
    type: Schema.ObjectId,
    ref: 'Label',
  }],// label id(if any lable is given to this card)
  account_id: {
    type: Schema.ObjectId,
    ref: 'AccountInfo'
  },//account id (this card is related to which account)
  category_id: {
    type: Schema.ObjectId,
    ref: 'Categories'
  },//category id (this policy deal is related to which category)
  custom_category : String,
  agent: [{
    type: Schema.ObjectId,
    ref: 'Agents'
  }],//agent id (this card is related to which agent)
  archived : {
    type : Boolean,
    default : false
  } ,//card is deleted or not
  checklist:[{}],
  // won : {
  //   type : Boolean,
  //   default : false
  // },
  //use for won/lost
  cardStatus : {
    type : String,
    default : 'inProcess'
  },//deal status (is this deal is done or not)
  cardStatusUpdatedAt:{
    type:Date,
    default: Date.now
  },//on which date this card is won or lost
  assignedTo:[{
        type: Schema.ObjectId,
        ref: 'Agents'
  }],//this card is assigned to which agent
  createdAt: {
    type: Date,
    default: Date.now
  },//creation date
  category_id: {
    type: Schema.ObjectId,
    ref: 'Categories'
  },
  user_defined_fields :[],
  smartContactInfo : {},
  mailThreadId:[],
  modulesNotRequired : [],
  insurance : {
    type: Schema.ObjectId,
    ref: 'Insurance'
  },//insurance id  (this card is related to which poilcy of assosiated user)
  // insurance_data :{
  //   type:Object,
  //   default : {}
  // },
  insurance_data:[],
  recentActivityOn:{},//what is the recent activity done on that card (like sms,email,activity etc)
  archivedAt: {
    type: Date
  },//deletion date
  archivedBy:{
    type: Schema.ObjectId,
    ref: 'Agents'
  },// who deleted this card
  due_date:{
    type : Date
  },// what is the due date of this card
  agency_id :{
    type: Schema.ObjectId,
    ref: 'Agency'
  },//agency id (this card is related to which agency (sharable))
  due_date_checked:{
    type : Boolean,
    default : false
  },//is this card or deal is done on marked date
  card_position : String,// what is the postion of the card in stage
  tags: [],
  external_source_info : {
    type: Schema.ObjectId,
    ref: 'Externalcontactinfo'
  },// external contact model id(if card came from outsource like website demo request, quote genereted)
  broker_fee: String,// what is the fees of the broker
  dealboard_info: {
    type: Schema.ObjectId,
    ref: 'Dealboardinfo'
   },
   deal_value:{
     type : String
   },// what is value of that deal
   importQuotesheet : {
     type : Boolean,
     default : false
   },// quotesheet is imported or not
   quoteCat : {},// what are the ctegories of the quote (auto, hometrucking etc),
   quoteLang : String,
   notes : String
});

module.exports= mongoose.model('Dealboardcard', DealboardcardSchema);
