'use strict';

const mongoose =require('mongoose');
var AgendajobsSchema = new mongoose.Schema({
  name: String,
  info: String,
  active: Boolean,
  data: mongoose.Schema.Types.ObjectId,
  type: String,
  priority: Number,
  lastRunAt: Date,
  lockedAt: Date,
  lastModifiedBy: Date,
  nextRunAt : Date,
  deleted: String
});

module.exports=mongoose.model('Agendajobs', AgendajobsSchema);
