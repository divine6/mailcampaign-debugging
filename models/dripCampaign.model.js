
const mongoose = require('mongoose');
const Schema = mongoose.Schema


var DripCampaignSchema = new mongoose.Schema({
  title: String,
  agent:{
    type: Schema.ObjectId,
    ref: 'Agents'
  },
  archivedAt :{
    type : Date
  },
  archived:{
    type:Boolean,
    default: false
  },
  sequenceType:String,
  createdAt:{
    type:Date,
    default:Date.now
  }
});
module.exports =  mongoose.model('DripCampaign', DripCampaignSchema);
