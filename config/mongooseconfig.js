const mongoose = require("mongoose")
const dbconfig = require("./dbconfig")


mongoose.connect(dbconfig.mongo.uri, dbconfig.mongo.options);
mongoose.connection.on("open", function(res) {
    console.log("Connected to mongo server.");
    require("./../models/index.model")    
    console.log("imported all models")
    //process.exit(-1); 
});
mongoose.connection.on('error', function(err) {
  console.error(`MongoDB connection error: ${err}`);
  process.exit(-1); // eslint-disable-line no-process-exit
});

